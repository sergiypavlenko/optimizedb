��             +         �     �     �     �     �     �  
   �     �           3     J     g  *   ~     �     �     �     �     �  C        S     g     �     �  ;   �  @   �  !   +  6   M  K   �  -   �     �       p   )  �  �     d     p     y     �  
   �     �  2   �  E   �  *   %	  7   P	  '   �	  R   �	  ,   
     0
  0   C
  #   t
  (   �
  z   �
  1   <  ?   n  #   �  0   �  V     �   Z  6   �  E     �   Z  B   �     +      J  �   k                                                              	                                                           
                       14 days 2 day 30 days 60 days 7 days @count day Alerts are not available Automatic clear cache_form table Clear cache_form table Clear cache_form table every Completed with errors. Current information on all database tables Database Optimization Disabled Executing commands manually Hide notification Information about tables Information on how to correctly perform when optimizing a database. Last run: @date ago Optimization settings database Optimize tables Optimized @count tables Optimizing the site database and clearing tables cache_form Receive notification of the need to optimize the database, every Startup and configuration module. The current size of the table <strong>@length</strong> The following message on the need to perform optimization, you get - @date. The size of all tables in the database: @size The table is cleared. When performing Cron You must perform <a href="@optimize_link">database optimization</a> . <a href="@hide_link">Hide notification</a> Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2013-05-30 08:00+0000
PO-Revision-Date: 2013-05-30 11:16+0200
Last-Translator: Sergei <sergiy.pavlenko@ya.ru>
Language-Team: Russian <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));
X-Generator: Poedit 1.5.5
 14 дней 2 дня 30 дней 60 дней 7 дней @count день Предупреждения не доступны Автоматически очистить таблицу cache_form Очистить таблицу cache_form Очистить таблицу cache_form каждый Выполнено с ошибками. Текущая информация о всех таблиц базы данных Оптимизация базы данных Отключено Выполнение команд вручную Скрыть уведомления Информация о таблицах Информация о том, как правильно выполнять оптимизацию базы данных. Последний запуск: @date назад Параметры оптимизации базы данных Оптимизация таблиц Оптимизировано @count таблиц Оптимизация базы данных и очистка таблиц cache_form Получение уведомления о необходимости оптимизации базы данных, каждый Запуск и конфигурация модуля. Нынешний размер таблицы <strong>@length</strong> Следующее сообщение о необходимости выполнять оптимизацию, вы получите - @date. Размер всех таблиц в базе данных: @size Таблица очищена. При выполнении Cron Необходимо выполнить <a href="@optimize_link">оптимизацию баз данных</a>. <a href="@hide_link">Скрыть уведомление</a> 